# 1.7.2 2019-11-30
Fix bug #1 which could occur when networking was not very reliable

# 1.7.1 2019-09-22
By popular request, refreshing can now be done with the Enter key

# 1.7.0 2019-09-18
Additional functionality in the menu:
* Copy a cow to your clipboard, or save it to a file
* Toggle a 'Maximum random'-mode, which allows you to also shuffle cows while shuffling quotes

# 1.6.3 2019-09-14
Hotfix for SVG icon not working in the latest version of GNOME Shell.

# 1.6.2 2019-09-13
Minor fix for complex quotation characters which sometimes conflicted with the text bubbles. Also moved Cowsay to 3.34 runtime.

# 1.6.1 2019-09-11
Minor fix for complex quotation characters which sometimes conflicted with the text bubbles. Also shortened the time that caches remain valid.

# 1.6.0 2019-09-08
Major rework of quotes:
* Quotes are now loaded from Reddit Showerthoughts. This means that there are many new options, and you can now also load a random quote by hitting the refresh button
* Quotes are limited to the top-100 of the last 7 days, there is no profanity check though, so application age rating has been updated

# 1.5.0 2019-09-03
Minor improvements to quotes:
* Quotes are now loaded asynchronously

# 1.4.0 2019-09-01
Minor improvements to quotes:
* They now stay around if the user doesn't provide any input
* They are cached for an hour, so we don't hug the server to death

# 1.3.0 2019-08-31
Additional functionality:
* Daily quotes akin to Fortune
* Better eye-mutation support
* UI tweaks. Fixed some listings

# 1.2.0 2019-08-14
Additional functionality:
* If you had kept your silence, you would have stayed a philosopher. Your cow can now think in private
* UI tweaks. More smooth padding and spacing

# 1.1.1 2019-08-11
Update for Flathub integration:
* Quick fix for Gnome, GNOME ambiguity
* Spelling fixes

# 1.1.0 2019-08-11
Update for Flathub integration:
* Changed application slug, so that it's more clear that this is no official GNOME application

# 1.0.0 2019-08-10
First Cowsay release, bringing you:
* Dropdown to select a cow of choice
* Extra selection options for eyes
* Free input field for your own text bubble
* Limited support for non ASCII characters

