/* cow-processor.vala
 *
 * Copyright 1999-2000 Tony Monroe
 * Copyright 2019 Kevin "Eonfge" Degeling
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Cowsay {

public struct CowOptions {
    public string model;
    public string eyes;
    public string voice;
    public bool tongue;
}

public class CowProcessor {

    private const string GP = GETTEXT_PACKAGE;

    private const int balloonWidth = 39;

    private Gee.Map<string, string> eyeMap;

    private Gee.Map<string, string> voiceMapSay;
    private Gee.Map<string, string> voiceMapThink;

    // Classical cowsay template regex
    private Regex regEyes;
    private Regex regTongue;
    private Regex regThoughts;
    private Regex regBalloon;

    // Word processing regex
    private Regex regWhiteSpace;

    public CowProcessor(){
        try{
            this.regEyes = new Regex ("{eye}");
            this.regTongue = new Regex ("{tongue}");
            this.regThoughts = new Regex ("{thoughts}");
            this.regBalloon = new Regex ("{the_cow}.*\n");

            this.regWhiteSpace = new Regex ("\\ ");
        } catch(Error e){
            return;
        }

        eyeMap = new Gee.HashMap<string, string> ();
        eyeMap.set ("normal", "o");
        eyeMap.set ("borg", "#");
        eyeMap.set ("dead", "x");
        eyeMap.set ("greedy", "$");
        eyeMap.set ("paranoid", "@");
        eyeMap.set ("stoned", "*");
        eyeMap.set ("tired", "-");
        eyeMap.set ("wired", "0");
        eyeMap.set ("young", ".");

        voiceMapSay = new Gee.HashMap<string, string> ();
        // For a single line
        voiceMapSay.set ("short_left", "< ");
        voiceMapSay.set ("short_right", " >");
        // For multi lines
        voiceMapSay.set ("left_bottom", "\\\\ ");
        voiceMapSay.set ("left_middle", "| ");
        voiceMapSay.set ("left_top", "/ ");
        voiceMapSay.set ("right_bottom", " /");
        voiceMapSay.set ("right_middle", " |");
        voiceMapSay.set ("right_top", " \\\\");

        voiceMapThink = new Gee.HashMap<string, string> ();
        // For a single line
        voiceMapThink.set ("short_left", "( ");
        voiceMapThink.set ("short_right", " )");
        // For multi lines
        voiceMapThink.set ("left_bottom", "( ");
        voiceMapThink.set ("left_middle", "( ");
        voiceMapThink.set ("left_top", "( ");
        voiceMapThink.set ("right_bottom", " )");
        voiceMapThink.set ("right_middle", " )");
        voiceMapThink.set ("right_top", " )");

    }

    public string getCow(CowOptions options, string sayingtext){
        string resource = "";

        try{
            resource = this.read_resource(options.model);
        } catch(Error e){
            return "Error occured";
        }

        string saying = this.cleanupCompexCharacters(sayingtext);

        try{
            // Decoration
            resource = regEyes.replace(resource, resource.length, 0, eyeMap.get(options.eyes));

            if(options.tongue == true){
                resource = regTongue.replace(resource, resource.length, 0, "U ");
            } else {
                resource = regTongue.replace(resource, resource.length, 0, "  ");
            }

            if(options.voice == "say"){
                resource = regThoughts.replace(resource, resource.length, 0, "\\\\");
            } else
            if(options.voice == "think"){
                resource = regThoughts.replace(resource, resource.length, 0, "o");
            }

            // Balloon
            string balloon = this.getTextBalloon(saying, options);

            // Add the balloon to the image
            resource = regBalloon.replace(resource, resource.length, 0, balloon);

        } catch(Error e){
            return "Error occured";
        }

        return resource;
    }

    private string getTextBalloon(string text, CowOptions options){

        if(text.length <= 0){
            text = "moow?";
        }

        // Example balloon
        //  _________________________________________
        // / Use 'dpkg --get-selections >            \
        // | selections.txt' to save a selection and |
        // | 'dpkg --set-selections < selections.txt |
        // \ && apt-get dselect-upgrade' to restore. /
        //  -----------------------------------------
        //  _______________
        // < one line test >
        //  ---------------
        //  _________________________________________
        // / hi there this should be about two lines \
        // \ of text                                 /
        //  -----------------------------------------
        //
        //  39 characters wide with two whitespace on both sides

        Gee.List<string> balloonList = new Gee.LinkedList<string>();

        // Make ballon text first, with a placeholder for the header
        // and footer.
        balloonList = this.makeTextBlock(text, balloonList);

        // Now lets put a ballon around it, replacing both the header, footer
        // and moving the strings back and forward a bit to add a balloon.
        balloonList = this.makeBalloonBlock(balloonList, options);

        // Convert it all to one long string so that it can be put on the screen
        // using a traditional terminal look.
        string balloonConcat = "";
        foreach(string row in balloonList){
            balloonConcat += row + "\n";
        }

        return balloonConcat;

    }

    private Gee.List<string> makeBalloonBlock(Gee.List<string> balloonList, CowOptions options){

        /* Decide what map we use based on the voice that the cow has.
         * this way, we have a nice and uniform way of styling the balloons.
         */
        Gee.Map<string, string> balloonMap = voiceMapSay;
        if(options.voice == "think"){
           balloonMap = voiceMapThink;
        }

        string headerReplace = " ";
        string footerReplace = " ";

        // TODO Make work with  diacritics like ', ", ’

        if(balloonList.size == 3){
            // Single row
            string textRow = balloonList.get(1);

            for(int i = 0; i < textRow.length + 2; i++){
                headerReplace += "_";
                footerReplace += "-";
            }

            balloonList.set(0, headerReplace);
            balloonList.set(2, footerReplace);


            textRow = balloonMap.get("short_left") + textRow + balloonMap.get("short_right");

            balloonList.set(1, textRow);

        } else
        if(balloonList.size >= 4){
            // Multi row
            for(int i = 0; i < balloonWidth + 2; i++){
                headerReplace += "_";
                footerReplace += "-";
            }

            balloonList.set(0, headerReplace);
            balloonList.set(balloonList.size - 1, footerReplace);

            int botRowIndex = balloonList.size - 2;

            string topRow = balloonList.get(1);
            string botRow = balloonList.get(botRowIndex);

            topRow = balloonMap.get("left_top") + topRow + padding(topRow) +  balloonMap.get("right_top");

            botRow = balloonMap.get("left_bottom") + botRow + padding(botRow) + balloonMap.get("right_bottom");

            balloonList.set(1, topRow);
            balloonList.set(botRowIndex, botRow);

            for(int i = 2; i < botRowIndex; i++){
                string middleRow = balloonList.get(i);
                middleRow = balloonMap.get("left_middle") + middleRow + padding(middleRow) + balloonMap.get("right_middle");
                balloonList.set(i, middleRow);
            }
        }

        return balloonList;
    }

    private string padding(string row){
        string rowPadding = "";
        for(int i = 0; i < balloonWidth - row.length; i++){
            rowPadding += " ";
        }
        return rowPadding;
    }

    private Gee.List<string> makeTextBlock(string text, Gee.List<string> balloonList){

        balloonList.add("HEADER");
        balloonList.add("");

        string[] words = regWhiteSpace.split(text);

        foreach(string word in words){

            int balloonRowIndex = balloonList.size - 1;

            string balloonRow = balloonList.get(balloonRowIndex);
            int balloonRowLength = balloonRow.length;

            if(word.length > balloonWidth){
                // TODO: Slice words and append over multiple lines
                // TODO: Make work with line breaks
            } else {

                if(balloonRowLength + word.length + 1 > balloonWidth){
                    // next row
                    balloonList.add(word._strip());
                } else {
                    balloonRow += " " + word;
                    balloonList.set(balloonRowIndex, balloonRow._strip());
                }
            }
        }
        balloonList.add("FOOTER");

        return balloonList;
    }

    private string read_resource(string name) throws Error {
        var resourceName = "cows." + name;

        InputStream input_stream = resources_open_stream(
            "/org/gnome/gitlab/Cowsay/" + resourceName,
            ResourceLookupFlags.NONE
        );

        DataInputStream data_stream = new DataInputStream(input_stream);
        size_t length;
        return data_stream.read_upto("\0", 1, out length);
    }

    private string cleanupCompexCharacters(string textstring) {
        string text = textstring.normalize();

        text = text.replace("’", "'");
        text = text.replace("‘", "'");
        text = text.replace("´", "'");
        text = text.replace("¨", "\"");
        text = text.replace("“", "\"");
        text = text.replace("”", "\"");
        text = text.replace("&amp;", "&");

        return text;
    }
}

}
