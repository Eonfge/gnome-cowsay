/* window.vala
 *
 * Copyright 2019 Kevin "Eonfge" Degeling
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Cowsay {

[GtkTemplate (ui = "/org/gnome/gitlab/Cowsay/window.ui")]
public class Window : Gtk.ApplicationWindow {

    private const string GP = GETTEXT_PACKAGE;

    [GtkChild]
    private unowned Gtk.TextView output;

    [GtkChild]
    private unowned Gtk.Entry input;

    [GtkChild]
    private unowned Gtk.ComboBoxText model;

    [GtkChild]
    private unowned Gtk.ComboBoxText eyes;

    [GtkChild]
    private unowned Gtk.ComboBoxText voice;

    [GtkChild]
    private unowned Gtk.Switch chaos;

    [GtkChild]
    private unowned Gtk.Button random;

    [GtkChild]
    private unowned Gtk.Button clipboard;

    [GtkChild]
    private unowned Gtk.Button save;

    [GtkChild]
    private unowned Gtk.Button about;

    [GtkChild]
    private unowned Gtk.Button help;

    private Cowsay.CowProcessor processor;
    private Cowsay.FortuneTeller teller;
    private Cowsay.Messenger messenger;

    private Gtk.Application app;
    private GLib.Settings settings;

    private string currentQuote;
    private bool easter;

    /*
     * For consistency, I chose not to use any <signal/> or similar in the window.ui
     * As such, all event-handlers and their implementation should be declared here.
     */
    public Window(Gtk.Application app) {
        Object(application: app);
        this.app = app;

        settings = new Settings("org.gnome.gitlab.Cowsay");

        chaos.state = settings.get_boolean("randomizer");
        chaos.state_set.connect( (state) => {
            settings.set_boolean("randomizer", state);
            return chaos.state = state;
        });

        processor = new Cowsay.CowProcessor();
        messenger = new Cowsay.Messenger();

        teller = new Cowsay.FortuneTeller(settings);

        teller.updateDailyQuotes();
        setLoadingMessage();

        model.changed.connect( () => {
             regenerateCow(input.text);
        });

        eyes.changed.connect( () => {
             regenerateCow(input.text);
        });

        input.changed.connect( () => {
            regenerateCow(input.text);
        });

        voice.changed.connect( () => {
            regenerateCow(input.text);
        });

        random.clicked.connect( () => {
            currentQuote = teller.getRandomQuote();
            randomizer(currentQuote);
        });

        clipboard.clicked.connect( () => {
            messenger.sendToClipboard(output.buffer.text, this.app);
        });

        save.clicked.connect( () => {
            messenger.sendToFileChooser(output.buffer.text, this.app, this);
        });

        about.clicked.connect( () => {
            showAboutPage();
        });

        help.clicked.connect( () => {
            // TODO: We must find a way to include the overlay, but GTK is rather
            // tightlipped about how to include these kind of files.
            var overlay = this.get_help_overlay();
            overlay.show();
        });

        // TODO: Get this working
        help.visible = false;

        currentQuote = teller.getRandomQuote();
        regenerateCow(currentQuote);
    }

    private void showAboutPage() {
        // This is an Easter egg that users can find
        easter = true;
        regenerateCow(currentQuote);

        Gdk.Pixbuf logo;
        try{
            logo = new Gdk.Pixbuf.from_resource("/org/gnome/gitlab/Cowsay/icons.cowsay.large");
        } catch(Error e){
            logo = null;
        }

        Gtk.show_about_dialog(
            this,
            "title", Statics.TITLE,
            "program-name", Statics.TITLE,
            "version", Statics.VERSION,
            "comments", Statics.MESSAGE,
            "copyright", Statics.COPYRIGHT,
            "website", Statics.WEBSITE,
            "website-label", Statics.WEBSITE_TITLE,
            "license_type", Gtk.License.GPL_3_0,
            "artists", Statics.ARTISTS,
            "authors", Statics.AUTHORS,
            "logo", logo,
            null
        );
    }

    public override bool key_press_event(Gdk.EventKey event) {
        var default_modifiers = Gtk.accelerator_get_default_mod_mask();

        // Quit
        if (event.keyval == Gdk.Key.q && (event.state & default_modifiers) == Gdk.ModifierType.CONTROL_MASK) {
            this.close();
            return true;
        } else

        // Copy
        if (event.keyval == Gdk.Key.c && (event.state & default_modifiers) == Gdk.ModifierType.CONTROL_MASK) {
            messenger.sendToClipboard(output.buffer.text, app);
            return true;
        } else

        // Save
        if (event.keyval == Gdk.Key.s && (event.state & default_modifiers) == Gdk.ModifierType.CONTROL_MASK) {
            messenger.sendToFileChooser(output.buffer.text, this.app, this);
            return true;
        } else

        // Random
        if(event.keyval == Gdk.Key.Return || event.keyval == Gdk.Key.KP_Enter)  {
            currentQuote = teller.getRandomQuote();
            randomizer(currentQuote);
        }

        return base.key_press_event(event);
    }

    private void randomizer(string text){

        if(chaos.active == true){
            var modelTreeLength = model.model.iter_n_children(null);
            model.set_active( Random.int_range (0, (modelTreeLength - 1) ) );

            var eyesTreeLength = eyes.model.iter_n_children(null);
            eyes.set_active( Random.int_range (0, (eyesTreeLength - 1) ) );
        }

        regenerateCow(text);
    }

    private void setLoadingMessage(){
        string json = settings.get_string("cache-data");

        if(json == null || json == ""){
            settings.changed.connect( () => {
                regenerateCow(Statics.COMPLETE);
            });
        }
    }

    private void regenerateCow(string text){
        string saying = text.strip();

        if(saying == null || saying == ""){
            saying = currentQuote;
        }

        CowOptions options = CowOptions();
        options.model = model.active_id;
        options.eyes = eyes.active_id;
        options.voice = voice.active_id;

        options.tongue = easter;

        string cow = processor.getCow(options, saying);

        // We set the window-size to 1x1 pixel so that the buffer-input will resize
        // it when the cow is added to the display.
        this.resize(1,1);

        output.buffer.text = cow;
    }

}

}

