/* statics.vala
 *
 * Copyright 2019 Kevin "Eonfge" Degeling
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Statics {

    private const string GP = GETTEXT_PACKAGE;

    //Translators: Keep Cowsay as brand-name, do not translate
    public const string TITLE = _("Cowsay");

    public const string VERSION = Config.PACKAGE_VERSION;

    public const string[] AUTHORS = {
        "Tony Monroe <tony@nog.net>",
        "Kevin \"Eonfge\" Degeling <contact@kevindegeling.nl>",
        null
    };

    public const string[] ARTISTS = {
        "Steven Barker <scbarker@uiuc.edu>",
        "Lars Smith <lars@csua.berkeley.edu>",
        "Krishna Kumar <krish.kumar@gmail.com>",
        "kube@csua.berkeley.edu",
        "Gürkan Sengün <gurkan@phys.ethz.ch>",
        "Geordan Rosario <geordan@csua.berkeley.edu>",
        "Gerfried Fuchs <rhonda@deb.at>",
        "aspolito@csua.berkeley.edu",
        "Nick Daly <nick.m.daly@gmail.com>",
        "{appel,kube,rowe}@csua.berkeley.edu",
        "chrysn <chrysn@fsfe.org>",
        "dpetrou@csua.berkeley.edu",
        "pborys@p-soft.silesia.linux.org.pl",
        "geordan@csua.berkeley.edu",
        "lim@csua.berkeley.edu",
        "Tony Maillefaud <maltouzes@gmail.com>",
        "Francois Marier <francois@debian.org>",
        "Gurkan Sengun <gurkan@phys.ethz.ch>",
        "Franz Pletz <fpletz@franz-pletz.org>",
        "Florian Ernst <florian@debian.org>",
        "Michael D. Ivey <ivey@debian.org>",
        "Nick Daly",
        null
    };

    public const string FALLBACK = _("Loading shower thoughts. If I can't think of anything witty, the burden is on you...");
    public const string COMPLETE = _("Loading shower thoughts. If I can't think of anything witty, the burden is on you... Loading complete. Hit the refresh button so I can inspire you.");

    public const string MESSAGE = _("""
        Cowsay is a very serious application about a talking cow, learning
        the GNOME ecosystem, and the Flatpak distribution network.

        Quotes supplied by Reddit Showerthoughts
    """);

    public const string COPYRIGHT = "Copyright © 2019-2021 Kevin \"Eonfge\" Degeling et al.";

    public const string WEBSITE = "https://gitlab.gnome.org/Eonfge/cowsay";
    public const string WEBSITE_TITLE = _("Official website");

}
