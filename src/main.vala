/* main.vala
 *
 * Copyright 2019 Kevin "Eonfge" Degeling
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const string GETTEXT_PACKAGE_CONTENT_MAIN = GETTEXT_PACKAGE;

int main (string[] args) {

    Intl.bindtextdomain(Config.PACKAGE_NAME, Config.PACKAGE_LOCALE);
    Intl.bind_textdomain_codeset(Config.PACKAGE_NAME, "UTF-8");
    Intl.textdomain(Config.PACKAGE_NAME);

    Gtk.init (ref args);

    DateTime appStart = new DateTime.now();

    var app = new Gtk.Application("org.gnome.gitlab.Cowsay", ApplicationFlags.FLAGS_NONE);

    app.activate.connect(() => {
        var win = app.active_window;

        if (win == null) {
            win = new Cowsay.Window(app);
            win.window_position = Gtk.WindowPosition.CENTER;
        }

        // Casting, like that, are you mad?
        win.present_with_time( (uint32) appStart.to_unix() );
        // Yes
    });

    return app.run(args);
}
