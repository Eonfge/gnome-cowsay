/* fortune-teller.vala
 *
 * Copyright 2019 Kevin "Eonfge" Degeling
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Cowsay {

/*
 * Here I'm going off the beaten path.
 *
 * See, Fortune has in the passed gained quite some controversy
 * for including quotes of less historically favoured people.
 *
 * This has caused quite the stirr in some communities, and I want
 * to sidestap that issue by just loading a quote from Reddit!
 * Main rationale: It gives me some networking experience, and it
 * carefully moves the responsiblity of 'what we quote' somewhere else.
 *
 * Let them sort it out, which quotes are pallatable for the masses.
 * And keep in mind that this application is rated Teen (12+) anyway
 * so any complaint will be piped to /dev/null
 */
errordomain FortuneException {
    LIMITED,
    UNKNOWN,
    CONNECTION
}

public class FortuneTeller{

    private const string GP = GETTEXT_PACKAGE;

    /* Is not actually private but I couldn't care less at this point.
     */
    private class Fortune{
        public string quote;
        public string author;

        public Fortune(string quote, string author){
            this.quote = quote;
            this.author = author;
        }
    }

    private string REDDIT_API = "https://www.reddit.com/r/showerthoughts/top.json?sort=top&t=week&limit=100";

    private Settings settings;

    public FortuneTeller(Settings _settings){
        settings = _settings;
    }

    public string getRandomQuote(){
        Fortune fortune = loadQuoteLocal();

        if(fortune == null){
            return Statics.FALLBACK;
        }

        return fortune.quote + " /u/" + fortune.author;
    }

    public void updateDailyQuotes(){

        /* I couldn't get that TimeSpan system working, for some reason the
         * span was always 1. Just that, so I gave up. This works and is simple
         * enough for our cause.
         */

        int64 quoteUnix = settings.get_int("cache-time");
        string existingJson = settings.get_string("cache-data");

        DateTime nowDate = new DateTime.now_local();
        int64 currentUnix = nowDate.to_unix();

        int64 diffUnix = currentUnix - quoteUnix;

        if(diffUnix > (12 * 60 * 60) || existingJson == ""){

            loadQuoteRemote.begin((obj, res) => {
                Gee.List<Fortune> freshCache = loadQuoteRemote.end(res);

                // Null means, unrecoverable, so likely a timeout
                if(freshCache != null && freshCache.size > 1) {

                    string json = listToString(freshCache);

                    settings.set_string("cache-data", json);
                    settings.set_int("cache-time", (int) nowDate.to_unix() );
                }
            });

        }
    }

    private Fortune? loadQuoteLocal() {
        string json = settings.get_string("cache-data");

        if(json == null || json == ""){
            return null;
        }

        Gee.List<Fortune> fortuneList = stringToList(json);

        if(fortuneList == null || fortuneList.size == 0){
            return null;
        }

        int length = fortuneList.size;
        int index = Random.int_range (0, (length - 1));

        return fortuneList.get(index);
    }

    private Gee.List<Fortune>? stringToList(string json){
        Gee.List<Fortune> fortuneList = new Gee.LinkedList<Fortune>();

        var parser = new Json.Parser();

        try {
            parser.load_from_data( json, -1);
        } catch (Error e){
            return null;
        }

        var root = parser.get_root();
        var root_array = root.get_array();

        foreach (var child in root_array.get_elements ()) {
            var fortuneJson = child.get_object();

            var quoteText = fortuneJson.get_string_member("quote");
            var quoteAuthor = fortuneJson.get_string_member("author");

            Fortune fortune = new Fortune(quoteText, quoteAuthor);
            fortuneList.add(fortune);
        }

        return fortuneList;
    }

    private string listToString(Gee.List<Fortune> fortuneList){

        var generator = new Json.Generator();

        var root = new Json.Node(Json.NodeType.ARRAY);
        var list = new Json.Array();

        foreach (Fortune fortune in fortuneList) {
            var object = new Json.Object();
            object.set_string_member("quote", fortune.quote);
            object.set_string_member("author", fortune.author);

            list.add_object_element(object);
        }

        root.set_array(list);
        generator.set_root(root);

        size_t length;
        string json = generator.to_data(out length);

        return json;
    }

    private async Gee.List<Fortune> loadQuoteRemote(){
        Gee.List<Fortune> fortuneList = new Gee.LinkedList<Fortune>();

        /* Threading is suprisingly hard. Who would have quessed...
         * I also found out that the 'async' keyword is much like in
         * JavaSCript: Syntextual sugar that doesn't actually make a method
         * asyncronous by itself. It only allows you to call the method with an
         * .begin() and .end(), wrapping threading in a nice anonymous function
         *
         * Read more here:
         * https://i-hate-farms.github.io/campfire/tutorials/async-and-threading.html
         * https://wiki.gnome.org/Projects/Vala/AsyncSamples
         */
        new Thread<void*> (null, () => {

            try {
                /* This callout system has gone through a few iterations.
                 * First I tried to do it myself, then I used curl as a hack,
                 * and then I found libsoup.
                 */
                var session = new Soup.Session();
                var message = new Soup.Message("GET", REDDIT_API);
                session.send_message(message);

                var parser = new Json.Parser();
                parser.load_from_data( (string) message.response_body.flatten().data, -1);

                var root = parser.get_root();
                var root_object = root.get_object();

                var responseData = root_object.get_object_member("data");
                var responseChildren = responseData.get_array_member("children");

                foreach (var child in responseChildren.get_elements ()) {
                    var quote = child.get_object();

                    var quoteData = quote.get_object_member("data");

                    var quoteText = quoteData.get_string_member("title");
                    var quoteAuthor = quoteData.get_string_member("author");

                    Fortune fortune = new Fortune(quoteText, quoteAuthor);
                    fortuneList.add(fortune);
                }


            } catch (Error e) {
                stderr.printf("loadQuoteRemote e: %s\n", e.message);
            }

            // Execute the callback
            Idle.add(loadQuoteRemote.callback);

            // Return thread value
            return null;

        });

        // Let async wait for thread to complete
        yield;

        // Return control to main thread and return result
        return fortuneList;
    }

}

}

