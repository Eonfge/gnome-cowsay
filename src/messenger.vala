/* messenger.vala
 *
 * Copyright 2019 Kevin "Eonfge" Degeling
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Cowsay {
/* Messenger is primarily made to separate the window and io logic
 * from eachother. You could just paste all of this in the
 * Window class, but that would quickly get quite messy.
 */
public class Messenger{

    private const string GP = GETTEXT_PACKAGE;

    public void sendToClipboard(string cow, Gtk.Application app){
        Gdk.Display display = Gdk.Display.get_default();
        Gtk.Clipboard clippy = Gtk.Clipboard.get_default(display);

        clippy.set_text(cow, cow.length);
        clippy.store();

        var note = new Notification(_("Creation copied to clipboard"));
        app.send_notification("clipboard", note);

        // Unless the user closes the app, remove the notification
        // after 10 seconds
        triggerDelay.begin (10000, (obj, async_res) => {
            app.withdraw_notification("clipboard");
        });
    }

    public void sendToFileChooser(string cow, Gtk.Application app, Gtk.Window context){

        var dialog = new Gtk.FileChooserNative(
            "Save File",
            context,
            Gtk.FileChooserAction.SAVE,
            "_Save",
            "_Cancel"
        );

        var datetime = new DateTime.now_local();

        dialog.set_current_name(
            datetime.format("%F")
            + "T"
            + datetime.format("%T")
            + "_cow"
        );


        bool accepted = (dialog.run() == Gtk.ResponseType.ACCEPT);

        string? filename = dialog.get_filename();
        dialog.destroy();

        if(accepted == false || filename == null || filename == ""){
            return;
        }

        try{
            File file = File.new_for_path(filename);
            FileOutputStream file_stream = file.replace(null, false, FileCreateFlags.NONE);

            var data_stream = new DataOutputStream(file_stream);
            data_stream.put_string(cow);
        } catch(Error e){
            var note = new Notification(_("Save action could not be preformed"));
            app.send_notification("saveFailed", note);

            // Unless the user closes the app, remove the notification
            // after 10 seconds
            triggerDelay.begin (10000, (obj, async_res) => {
                app.withdraw_notification("saveFailed");
            });
        }

        return;
    }

    private async void triggerDelay(uint milliseconds) {
        yield nap(milliseconds);
    }

    private async void nap(uint interval, int priority = GLib.Priority.DEFAULT) {
        GLib.Timeout.add(interval, () => {
            nap.callback();
            return false;
        }, priority);
        yield;
    }

}

}

