#!/usr/bin/env bash

clear

# Build
flatpak-builder --repo=testing-repo --force-clean build-dir org.gnome.gitlab.Cowsay.json

# Make a testing repository
flatpak --user remote-add --if-not-exists --no-gpg-verify cow-testing-repo testing-repo
flatpak --user install cow-testing-repo org.gnome.gitlab.Cowsay -y
flatpak --user install cow-testing-repo org.gnome.gitlab.Cowsay.Debug -y

# Update system
flatpak update -y

# Create single-install package
flatpak build-bundle testing-repo cowsay.flatpak org.gnome.gitlab.Cowsay

